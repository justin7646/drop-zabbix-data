package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"os"
)

type Config struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Db       string `yaml:"db"`
}

var cfg Config

func Init(cfgFile string) {
	if cfgFile == "" {
		cfgFile = `./config.yaml`
	}

	b, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		fmt.Fprintln(os.Stderr, "配置文件读取错误", err.Error())
		os.Exit(-1)
	}

	if err := yaml.Unmarshal(b, &cfg); err != nil {
		fmt.Fprintln(os.Stderr, "配置文件解析失败："+err.Error())
		os.Exit(-1)
	}

	if cfg.Host == ""  {
        cfg.Host = "127.0.0.1"
	}

	if cfg.Port == ""  {
		cfg.Port = "3306"
	}

	if cfg.User == ""  {
		fmt.Fprintln(os.Stderr,"User 需要设置！")
		os.Exit(-1)
	}

	if cfg.Password == ""  {
		fmt.Fprintln(os.Stderr,"Password 需要设置！")
		os.Exit(-1)
	}

	if cfg.Db == "" {
		cfg.Db = "zabbix"
	}
}

func GetCfg() Config {
	return cfg
}
