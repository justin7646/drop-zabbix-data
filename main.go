package main

import (
	"cleanMysqlPartition/config"
	"cleanMysqlPartition/pkg/operator"
	"flag"
)

func init()  {
	var configFile string
	flag.StringVar(&configFile, "f", "config.yaml", "config file")
	flag.Parse()
	config.Init(configFile)
}

func main()  {
	operator.NewOpear().DropPartition()
}
