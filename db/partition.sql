-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: zabbix
-- ------------------------------------------------------
-- Server version        8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping events for database 'zabbix'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `CreatePartition` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = '' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`192.168.10.6`*/ /*!50106 EVENT `CreatePartition` ON SCHEDULE EVERY 1 DAY STARTS '2021-01-01 01:00:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
            call proc_create_partition_all();
        END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'zabbix'
--
/*!50003 DROP PROCEDURE IF EXISTS `proc_create_partition_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_create_partition_all`()
BEGIN
  DECLARE tbname varchar(32);
  DECLARE done INT DEFAULT FALSE ;
  #查询已按月分区的表
  DECLARE part_cursor CURSOR FOR (SELECT DISTINCT table_name FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_SCHEMA = DATABASE() AND table_name NOT IN ('history','history_uint') AND partition_expression IS NOT NULL AND table_name NOT LIKE '%bak');
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  #循环对按月分区表添加分区
  OPEN part_cursor;
    myLoop: LOOP
      FETCH part_cursor INTO tbname;
      IF done THEN
        LEAVE myLoop;
      END IF;
      #调用分区存储过程
      CALL proc_create_partition_month(tbname);
      COMMIT;
    END LOOP myLoop;
  CLOSE part_cursor;

  #处理按日分区的表，目前有"history,history_uint"2张表按日分区
  CALL proc_create_partition_day("history");
  COMMIT;

  CALL proc_create_partition_day("history_uint");
  COMMIT;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_create_partition_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_create_partition_day`(in_tbname VARCHAR(64))
BEGIN
    SELECT DATABASE() INTO @dbname;
    SET @tbname = in_tbname;
    #查询表的最近一次分区编号
    SELECT
      partition_name INTO @PMAX
    FROM
      INFORMATION_SCHEMA.PARTITIONS
    WHERE
      TABLE_SCHEMA = @dbname
    AND table_name = @tbname
    ORDER BY
      partition_ordinal_position DESC
    LIMIT 1;
    #查询表的最近一次分区时间值
    SELECT
      partition_description INTO @PRETIMESTAMP
    FROM
      INFORMATION_SCHEMA.PARTITIONS
    WHERE
      TABLE_SCHEMA = @dbname
    AND table_name = @tbname
    ORDER BY
      partition_ordinal_position DESC
    LIMIT 1, 1;
    IF UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL 7 DAY)) > @PRETIMESTAMP
    THEN
      # 确定下一分区时间范围
      SET @NEXTDATE = DATE_ADD(FROM_UNIXTIME(@PRETIMESTAMP),INTERVAL 1 DAY);
      SET @t=CONCAT('alter table `',@dbname,'`.',@tbname,' reorganize partition ',@PMAX,
      ' into(partition ',@PMAX,' values less than (UNIX_TIMESTAMP("',DATE_FORMAT(@NEXTDATE,'%Y-%m-%d'),'")),',
      'partition p',DATE_FORMAT(@NEXTDATE,'%Y%m%d'),' values less than (MAXVALUE))');
      SELECT @t;
      PREPARE stmt FROM @t;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
      COMMIT;
    END IF;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_create_partition_month` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_create_partition_month`(in_tbname VARCHAR(64))
BEGIN
    SELECT DATABASE() INTO @dbname;
    SET @tbname = in_tbname;
    #查询表的最近一次分区编号
    SELECT
      partition_name INTO @PMAX
    FROM
      INFORMATION_SCHEMA.PARTITIONS
    WHERE
      TABLE_SCHEMA = @dbname
    AND table_name = @tbname
    ORDER BY
      partition_ordinal_position DESC
    LIMIT 1;
    #查询表的最近一次分区时间值
    SELECT
      partition_description INTO @PRETIMESTAMP
    FROM
      INFORMATION_SCHEMA.PARTITIONS
    WHERE
      TABLE_SCHEMA = @dbname
    AND table_name = @tbname
    ORDER BY
      partition_ordinal_position DESC
    LIMIT 1, 1;
    IF UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL 1 MONTH)) > @PRETIMESTAMP
    THEN
      # 如1月应建立2月的分区，当前2月属于MAXVALUE区未划分出来，时间截止3月1日前
      SET @NEXTDATE = DATE_ADD(FROM_UNIXTIME(@PRETIMESTAMP),INTERVAL 1 MONTH);
      SET @t=CONCAT('alter table `',@dbname,'`.',@tbname,' reorganize partition ',@PMAX,
      ' into(partition ',@PMAX,' values less than (UNIX_TIMESTAMP("',DATE_FORMAT(@NEXTDATE,'%Y-%m-%d'),'")),',
      'partition p',DATE_FORMAT(@NEXTDATE,'%Y%m'),' values less than (MAXVALUE))');
      SELECT @t;
      PREPARE stmt FROM @t;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
      COMMIT;
    END IF;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_drop_partition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_drop_partition`()
BEGIN
    DECLARE v_sql VARCHAR(500);
    DECLARE v_done INT DEFAULT 0;
    declare v_table_name varchar(50);
    declare v_partition_name varchar(50);
    declare v_partition_description varchar(300);
    DECLARE v_cursor CURSOR FOR
        SELECT p.TABLE_NAME,
               p.PARTITION_NAME,
               p.PARTITION_DESCRIPTION
        FROM information_schema.PARTITIONS p
        WHERE p.TABLE_SCHEMA = 'zabbix'
          AND p.PARTITION_NAME IS NOT NULL
          AND p.PARTITION_DESCRIPTION != 'MAXVALUE'
          AND CAST(p.PARTITION_DESCRIPTION AS SIGNED) < (IF(p.TABLE_NAME IN ('history', 'history_uint'),
                                                            UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 60 DAY)),
                                                            UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 360 DAY))));
    declare continue handler for sqlstate '02000' set v_done = 1;
    open v_cursor;
    fetch v_cursor into v_table_name,v_partition_name,v_partition_description;
    while v_done = 0
        do
            set v_sql = concat('alter table ', v_table_name, ' drop partition ', v_partition_name);
            prepare stat from @v_sql;
            execute stat;
            deallocate prepare stat;
            fetch v_cursor into v_table_name,v_partition_name,v_partition_description;
        end while;
    close v_cursor;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-30 17:23:15
