package conn

import (
	"cleanMysqlPartition/config"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/reflectx"
	"os"
	"strings"
)

func GetDbConn() *sqlx.DB {
	dbUrl := config.GetCfg().User + `:` + config.GetCfg().Password + `@tcp(` + config.GetCfg().Host + ":" + config.GetCfg().Port + ")/" + config.GetCfg().Db + "?charset=utf8"
	db, err := sqlx.Open("mysql", dbUrl)
	if err != nil {
		fmt.Fprintln(os.Stderr, "无法连接数据库服务器："+err.Error())
		os.Exit(-1)
	}
	err = db.Ping()
	if err != nil {
		fmt.Fprintln(os.Stderr, "无法连接数据库服务器："+err.Error())
		os.Exit(-1)
	}
	db.Mapper = reflectx.NewMapperFunc("db", strings.ToUpper)
	return db
}
