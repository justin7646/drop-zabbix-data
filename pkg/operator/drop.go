package operator

import (
	"cleanMysqlPartition/config"
	"cleanMysqlPartition/pkg/conn"
	"fmt"
	"os"
	"strconv"
)

type Partition struct {
	TableName string  `db:"TABLE_NAME"`
	PartName  string  `db:"PARTITION_NAME"`
	PartDesc  string  `db:"PARTITION_DESCRIPTION"`
}

func (p Partition)drop()  {
	db := conn.GetDbConn()
	sql := `alter table `+p.TableName + ` drop partition `+p.PartName
	_,err := db.Exec(sql)
	if err != nil {
		fmt.Fprintln(os.Stderr,err.Error())
	}else{
		fmt.Fprintln(os.Stdout,"成功删除"+p.TableName +"的分区："+p.PartName)
	}
}

type opera struct {
	sql     string
	parList []Partition
}

func NewOpear() *opera {
	o := opera{}
	o.sql = `SELECT p.table_name, p.partition_name, p.partition_description
               FROM information_schema.PARTITIONS p
			  WHERE p.TABLE_SCHEMA = '`+config.GetCfg().Db +`'
  			    AND p.PARTITION_NAME IS NOT NULL
  				AND p.PARTITION_DESCRIPTION != 'MAXVALUE'
  				AND CAST(p.PARTITION_DESCRIPTION AS SIGNED) <
      					(IF(p.TABLE_NAME IN ('history', 'history_uint'), 
                            UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 60 DAY)),
          					UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 360 DAY))))`

	return &o
}

func (o *opera)list(){
	db := conn.GetDbConn()
	err := db.Select(&o.parList,o.sql)
	if err != nil {
		fmt.Fprintln(os.Stderr,"查询失败："+err.Error())
	}

}

func (o *opera)DropPartition()  {
	o.list()
	if o.parList !=nil && len(o.parList) > 0{
		fmt.Fprintln(os.Stdout,"查出了需要清理的表空间："+ strconv.Itoa(len(o.parList)) +"个")
        for _,p := range o.parList{
        	p.drop()
		}
		fmt.Fprintln(os.Stdout,"命令成功执行！")
	}else{
		fmt.Fprintln(os.Stdout,"没有需要清理的表空间！")
	}
}