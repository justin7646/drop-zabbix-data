module cleanMysqlPartition

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/spf13/cobra v1.1.3
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
